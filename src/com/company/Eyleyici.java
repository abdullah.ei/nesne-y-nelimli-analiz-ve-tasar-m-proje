package com.company;

public class Eyleyici implements IEyleyici{
    private static Eyleyici eyleyici = new Eyleyici();
    private boolean acık = false;

    private Eyleyici() {}

    public static Eyleyici getEyleyici()
    {
        if(eyleyici == null)
            eyleyici = new Eyleyici();
        return eyleyici;
    }

    public void sogutucuAc(){
        System.out.println("Sogutucu soğutmaya başladı...");
        acık = true;
    }

    public void sogutucuKapat()
    {
        System.out.println("Sogutucu kapatıldı.");
        acık = false;
    }
}
