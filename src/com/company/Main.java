package com.company;

import java.util.Scanner;

public class Main {

    // Uygulama
    public static void main(String[] args) {
        // Cihaza erişmek için bir api sayılabilir.
        IAgArayuzu agArayuzu = new AgArayuzu();

        System.out.println("Akıllı cihaza giriş yapın: ");
        Scanner i = new Scanner(System.in);

        //giriş ekranı
        while (true)
        {
            System.out.print("Isim: ");
            String isim = i.next();
            System.out.print("Sifre: ");
            String sifre = i.next();

            if (!agArayuzu.giris(isim,sifre))
            {
                System.out.println("şifre ve kullanıcı adı yanlış.");
            } else {
                System.out.println("Hoşgeldin "+isim+", sisteme giriş yapıldı");
                break;
            }
        }

        // kontrol paneli
        while (true)
        {
            System.out.println("1. soğutucyu aç");
            System.out.println("2. soğutucyu kapat");
            System.out.println("3. sıcaklığı göster");
            System.out.println("0. çıkış");
            switch (i.nextInt())
            {
                case 1:
                    agArayuzu.sogutucuAc();
                    break;
                case 2:
                    agArayuzu.sogutucuKapat();
                    break;
                case 3:
                    System.out.println("Sicaklik: "+agArayuzu.sicaklikGetir());
                    break;
                case 0:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Geçersiz seçenek");
            }
        }

    }

}
