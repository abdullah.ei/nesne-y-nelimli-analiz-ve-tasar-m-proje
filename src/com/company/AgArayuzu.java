package com.company;

public class AgArayuzu implements IAgArayuzu{
    private Veritabani veritabani;
    private IEyleyici eyleyici;
    private ISicaklikAlgilayici sicaklikAlgilayici;
    // kullanıcının girmiş olup olmadığını kontrol eden bir değişken
    // gerçek hayatta farklı mekanizmalar kullanılır.
    private boolean yetki  = false ;

    public AgArayuzu()
    {
        this.veritabani = Veritabani.veritabaniGetir();
        this.eyleyici = Eyleyici.getEyleyici();
        this.sicaklikAlgilayici = SicaklikAlgilayici.getSicaklikAlgilayici();
    }

    public boolean giris(String isim,String sifre)
    {
        if (veritabani.kullaniciDogrula(isim, sifre))
        {
            yetki = true;
            return true;
        }

        return false;
    }

    public void sogutucuAc()
    {
        if (yetki)
            this.eyleyici.sogutucuAc();
        else hata();
    }

    public  void sogutucuKapat()
    {
        if (yetki)
            this.eyleyici.sogutucuKapat();
        else hata();
    }

    public int sicaklikGetir()
    {
        if (yetki)
            return this.sicaklikAlgilayici.sicaklikGetir();
        else
            return 0;
    }

    private void hata()
    {
        System.out.println("Erişim yetkiniz yok.");
    }
}
