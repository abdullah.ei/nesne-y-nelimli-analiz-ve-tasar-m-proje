package com.company;

import java.util.Random;

public class SicaklikAlgilayici implements ISicaklikAlgilayici {
    private Random random;
    private static SicaklikAlgilayici a;

    private SicaklikAlgilayici()
    {
        random = new Random();
    }

    public static SicaklikAlgilayici getSicaklikAlgilayici()
    {
        if (a==null)
        {
            a = new SicaklikAlgilayici();
        }
        return a;
    }

    public int sicaklikGetir()
    {
        return random.nextInt(60);
    }
}
